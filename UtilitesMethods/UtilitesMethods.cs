﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using SpendaTestCases.config;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace SpendaTestCases.SetMethods
{
    class UtilitesMethods
    {
        //Enter Text
        public static void EnterText(IWebElement element, string value)
        {
            element.SendKeys(value);
        }

        //SelectTextDDL
        public static void SelectTextDDL(IWebElement element, string value)
        {
            new SelectElement((element)).SelectByText(value);
           
        }
        //click into a button 
        public static void Click(IWebElement element)
        {
            element.Click();
        }
        //Get Text
        public static string GetText(IWebElement element)
        {
            return element.GetAttribute("value");

        }
        //GetTextDDL
        public static string GetTextFromDDL(IWebElement element, PropertyType elementType)
        {
            return new SelectElement(element).AllSelectedOptions.SingleOrDefault().Text;
        }

        //GetRadioButton Value

        public static string GetRadioButtonValue(string element, string elementType)
        {
            IList<IWebElement> rdBtn = PropertiesCollection.driver.FindElements(By.Name(element));
            Boolean bValue = false;
            bValue = rdBtn.ElementAt(0).Selected;
            if (bValue == true)
            {
                return rdBtn.ElementAt(1).GetAttribute("value");
            }
            else
            {
                return rdBtn.ElementAt(0).GetAttribute("value");
            }

        }
        public static void WaitUntilElementExists(int timeout = 30)
        {
            
            try
            {
                //IWebDriver driver = new ChromeDriver();
                //var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(timeout));
                //return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions);
                PropertiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeout);
                Thread.Sleep(3000);
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element was not found in current context page.");
                throw;
            }
        }
        
    }
}
