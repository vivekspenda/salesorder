﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendaTestCases.Pages
{
    public static class Constants
    {
        public static string SpendaLogin = "https://featweb3-sw-app.azurewebsites.net/";
        public static string SpendaLoginUser = "nikhil.jangid@spenda.co";
        public static string SpendaLoginPass = "1qwerty";
        public static string ContactEmailAddress = "neelgupta@gmail.com";
        public static string ContactPhone = "7737117922";
        public static string CompanyName = "WELLONE Enterprises";
        public static string AccountRef = "spenda";
        public static string ABN = "12345678912";
        public static string PhoneNumber = "1234567890";
        public static string FirstName = "Neel";
        public static string LastName = "Gupta";
        public static string EmailAddress = "neelgupta@gmail.com";
        public static string MobileNumber = "7737117921";
        public static string LocationName = "India";
        public static string Address = "Rajat";
        public static string TrackingUrl = "orderspenda.";
        

    }
}
