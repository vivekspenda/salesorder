﻿using OpenQA.Selenium.Chrome;
using System.Configuration;
using WebDriverManager.DriverConfigs.Impl;

namespace SpendaTestCases.config
{
    public static class ConfigHelper
    {
        public static ChromeDriver GetDriver()
        {
            string DriverPath = ConfigurationManager.AppSettings["driverPath"];
            new WebDriverManager.DriverManager()
                           .SetUpDriver(new ChromeConfig());

            return new ChromeDriver(DriverPath);
        }
    }
}