﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SpendaTestCases.config;



namespace SpendaTestCases.PageObjects
{
    public class DashboardPageObjects
    {

        public DashboardPageObjects()
        {
            PageFactory.InitElements(ConfigHelper.GetDriver(), this);
        }
        //Using FindBy for locating elements
        [FindsBy(How = How.XPath, Using = "//p[text()='Sales Orders']")]
        public IWebElement SalesOrderMenu { get; set; }
        

    }
}
