﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SpendaTestCases.config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendaTestCases.PageObjects
{
   public class CreateNewOrderPageobjects
    {
        public CreateNewOrderPageobjects()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
        [FindsBy(How = How.XPath, Using = "//p[text()='Sales Orders']")]
        public IWebElement SalesOrderMenu { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Create Order']")]
        public IWebElement BtnCreateOrder { get; set; }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]/*[name()='svg'][1]")]
        public IWebElement BtnSelectCustomer { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@id='react-autowhatever-1--item-0']")]
        public IWebElement CustomerNameDDL { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Discard Order']")]
        public IWebElement BtnDiscardOrder { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='General Details']")]
        public IWebElement TabGeneralDetails { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[normalize-space()='Order Details']")]
        public IWebElement TabOrderDetails { get; set; }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/button[1]/span[1]")]
        public IWebElement BtnEditOrderDetials { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='MuiIconButton-label']//*[name()='svg']")]
        public IWebElement BtnSelectOrderItems { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@id='react-autowhatever-1--item-2']//div[@class='relative']")]
        public IWebElement SelectOrderItemsFromDDL { get; set; }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[3]/div[1]/div[1]/input[1]")]
        public IWebElement Price { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='pendingQty-NewLineItem_3']")]
        public IWebElement Qty { get; set; }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/button[2]/span[1]")]
        public IWebElement BtnDone { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='MuiButton-label-29360']")]
        public IWebElement AddLineItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Order Details']")]
        public IWebElement SearchCustomerDDL { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='ContactEmailAddress']")]
        public IWebElement ContactEmailAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='ContactPhone']")]
        public IWebElement ContactPhone { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='CompanyName']")]
        public IWebElement CompanyName { get; set; }

        [FindsBy(How = How.XPath, Using = "//textarea[@id='InternalNote']")]
        public IWebElement AddNotes { get; set; }

        //Select Shipping and Billing address Popup
        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/div[1]/*[name()='svg'][1]/*[name()='path'][1]")]
        public IWebElement ShippingAddressEdit { get; set; }

        [FindsBy(How = How.XPath, Using = "//tr[@class='align-top relative']//div[@class='absolute cursor-pointer']//*[name()='svg']")]
        public IWebElement BillingAddressEdit { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='MuiBox-root jss2806']//div[2]//*[name()='svg']")]
        public IWebElement BtnDelAddress { get; set; }
        

        [FindsBy(How = How.XPath, Using = "//span[text()='Add New Address']")]
        public IWebElement btnAddNewAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='free-solo-dialog-demo']")]
        public IWebElement InputLocationName { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@id='free-solo-dialog-demo-option-0']")]
        public IWebElement SelectLocationName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search Location']")]
        public IWebElement SearchLocationAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//p[contains(text(),'Rajat Path, Narayan Pura, Mansarovar Sector 6, Man')]")]
        public IWebElement SelectAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Save']")]
        public IWebElement BtnSave { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Done']")]
        public IWebElement Donebtn { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//span[text()='Cancel']")]
        public IWebElement BtnCancel { get; set; }

    }
}
