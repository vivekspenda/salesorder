﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SpendaTestCases.config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendaTestCases.PageObjects
{
    class CreateNewCustomerPageOjects
    {
        public CreateNewCustomerPageOjects()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/button[2]/span[1]/*[name()='svg'][1]/*[name()='path'][1]")]
        public IWebElement CreatenewCustomer { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Name']")]
        public IWebElement CompanyName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='RefNumber']")]
        public IWebElement AccountRef { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='ABN']")]
        public IWebElement ABN { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Phone1']")]
        public IWebElement PhoneNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Contacts.[0].FirstName']")]
        public IWebElement FirstName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Contacts.0.LastName']")]
        public IWebElement LastName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Contacts.0.EmailAddress']")]
        public IWebElement EmailAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Contacts.0.Phone1']")]
        public IWebElement MobileNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[4]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[7]/div[1]/button[1]/span[1]")]
        public IWebElement AddContact { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='MuiButtonBase-root MuiIconButton-root jss54736']//span[@class='MuiIconButton-label']//*[name()='svg']")]
        public IWebElement DeleteContact { get; set; }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[4]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[9]/div[2]/button[1]/span[1]")]
        public IWebElement AddLocationbtn { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='Locations.0.Name']")]
        public IWebElement LocationName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search Location']")]
        public IWebElement Address { get; set; }

        [FindsBy(How = How.XPath, Using = "//p[contains(text(),'Rajat Path, Narayan Pura, Mansarovar Sector 6, Man')]")]
        public IWebElement SelectAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Save Address']")]
        public IWebElement SaveAddress { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//span[text()='Create']")]
        public IWebElement btnCreate { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[name()='path' and contains(@d,'M19 6.41L1')]")]
        public IWebElement ClosePopup { get; set; }
        
    }

}
