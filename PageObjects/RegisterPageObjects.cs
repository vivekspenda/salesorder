﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using SpendaTestCases.config;
using SpendaTestCases.SetMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendaTestCases.Testscases.RegisterPage
{
    class RegisterPageObjects
    {
        public RegisterPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        //Using FindBy for locating elements
        [FindsBy(How = How.XPath, Using = "//input[@data-autoid='txtCompanyName']")]
        public IWebElement companyName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@data-autoid='txtEmailAddress']")]
        public IWebElement emailAddress { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@data-autoid='txtFirstName']")]
        public IWebElement firstName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@data-autoid='txtLastName']")]
        public IWebElement lastName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@type='password']")]
        public IWebElement password { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@data-autoid='txtConfirmPassword']")]
        public IWebElement confirmPassword { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@data-autoid='chkAgreeTnCs']")]
        public IWebElement agreeTnCs { get; set; }
        [FindsBy(How = How.XPath, Using = "//span[@class='MuiButton-label']")]
        public IWebElement btnRegister { get; set; }

        public void Login(string companyN, string emailAddress)
        {
            UtilitesMethods.EnterText(companyName, companyN);
            //UtilitesMethods.EnterText(emailAddress, password);
            UtilitesMethods.Click(btnRegister);
        }
    }
}
