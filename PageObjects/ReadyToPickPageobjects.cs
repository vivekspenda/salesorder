﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SpendaTestCases.config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendaTestCases.PageObjects
{
    class ReadyToPickPageobjects
    {
        public ReadyToPickPageobjects()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//p[text()='Sales Orders']")]
        public IWebElement SalesOrderMenu { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Require Attention']")]
        public IWebElement RequireAttention { get; set; }

        [FindsBy(How = How.XPath, Using = "//tbody/tr[1]/td[2]")]
        public IWebElement SelectOrder { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Ready To Pick']")]
        public IWebElement BtnReadytoPick { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Print Pick Slip']")]
        public IWebElement BtnPrintPickSlip { get; set; }

        [FindsBy(How = How.XPath, Using = "//img[@src='/static/media/left-angle.7751b06e.svg']")]
        public IWebElement BtnOpenStatusbar { get; set; }

        [FindsBy(How = How.XPath, Using = "//img[@src='/static/media/right-angle.91428552.svg']")]
        public IWebElement BtnCloseStatusbar { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//span[text()='Move to Packaging']")]
        public IWebElement BtnMovetoPackaging { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Pack']")]
        public IWebElement BtnPack { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Ready To Ship']")]
        public IWebElement BtnReadytoShip { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='www.tracking.url']")]
        public IWebElement TrackingUrl { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Tracking ID']")]
        public IWebElement TrackingId { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Submit & Ship']")]
        public IWebElement BtnSubmitandShip { get; set; }
        
    }
}
