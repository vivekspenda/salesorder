﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SpendaTestCases.config;
using SpendaTestCases.SetMethods;

namespace SpendaTestCases.Testscases.Loginpage
{
     class LoginPageObjects
    {
        public LoginPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
        //Using FindBy for locating elements
        [FindsBy(How = How.XPath, Using = "//input[@id='username']")]
        public IWebElement txtEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='password']")]
        public IWebElement txtPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='LogIn']")]
        public IWebElement btnSignIn { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@data-autoid='btnSetupTwoStepAuthLater']")]
        public IWebElement btnSetupTwoStepAuthLater { get; set; }

        


        public void Login(string username, string password)
        {
            UtilitesMethods.EnterText(txtEmail, username);
            UtilitesMethods.EnterText(txtPassword, password);
            UtilitesMethods.Click(btnSignIn);
        }
    }
}
