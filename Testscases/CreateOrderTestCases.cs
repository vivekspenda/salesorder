﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpendaTestCases.config;
using SpendaTestCases.Pages;
using SpendaTestCases.Testscases.Loginpage;
using System;
using SpendaTestCases.SetMethods;
using SpendaTestCases.PageObjects;
using OpenQA.Selenium.Interactions;

namespace SpendaTestCases.Testscases
{

    public class CreateOrderTestCases
    {
        static ChromeDriver driver = ConfigHelper.GetDriver();
        CreateNewOrderPageobjects CreateNewOrderEC;
        CreateNewCustomerPageOjects CreateNewCustomer;
        ReadyToPickPageobjects Processorder;
        [SetUp]
        public void Initailize()
        {
            Console.Write("login test case started ");
            PropertiesCollection.driver = driver;
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Constants.SpendaLogin);

            CreateNewCustomer = new CreateNewCustomerPageOjects();
            CreateNewOrderEC = new CreateNewOrderPageobjects();
            Processorder = new ReadyToPickPageobjects();
        }


        //Check User Login Functionality
        [Test, Order(0), Category("CreateOrderTestCases")]
        public void UserLogin()
        {
            Console.Write("Preformed login");
            LoginPageObjects LoginPage = new LoginPageObjects();
            UtilitesMethods.WaitUntilElementExists();
            LoginPage.txtEmail.SendKeys(Constants.SpendaLoginUser);
            LoginPage.txtPassword.SendKeys(Constants.SpendaLoginPass);
            LoginPage.btnSignIn.Click();
            Console.Write("clicked on login button");
            UtilitesMethods.WaitUntilElementExists();
            LoginPage.btnSetupTwoStepAuthLater.Click();
            Console.Write("clicked on Setup Two Step Auth Later button");
        }

        // Create new sales order with existing customer.
        [Test, Order(1), Category("CreateOrderTestCases")]
        public void CreateNewOrderWithExistingCustomer()
        {
            UtilitesMethods.WaitUntilElementExists();

            // clicked on sales order Menu on dashboard
            if (CreateNewOrderEC.SalesOrderMenu.Displayed)
            {
                CreateNewOrderEC.SalesOrderMenu.Click();
                Console.Write("clicked on Sales Order Menu");
            }
            else
            {
                Console.Write("clicked on sales order Menu on dashboard is not displayed");
            }
            // clicked on Create Order Button
            if (CreateNewOrderEC.BtnCreateOrder.Displayed)
            {
                CreateNewOrderEC.BtnCreateOrder.Click();
                Console.Write("clicked on Create Order Button");
            }
            else
            {
                Console.Write("Create order button is not displayed");
            }
            // clicked on customer name DDL
            if (CreateNewOrderEC.BtnSelectCustomer.Displayed)
            {
                CreateNewOrderEC.BtnSelectCustomer.Click();
                Console.Write("clicked on customer name DDL");
                UtilitesMethods.WaitUntilElementExists();
            }
            else
            {
                Console.Write("Select customer arrow is not displayed");
            }

            if (CreateNewOrderEC.CustomerNameDDL.Displayed)
            {
                CreateNewOrderEC.CustomerNameDDL.Click();
                Console.Write("select customer name from DDL");
                UtilitesMethods.WaitUntilElementExists();
            }
            else
            {
                Console.Write("customer name list is not displayed");
            }

            if (string.IsNullOrEmpty(UtilitesMethods.GetText(CreateNewOrderEC.ContactEmailAddress)))
            {
                CreateNewOrderEC.ContactEmailAddress.SendKeys(Constants.ContactEmailAddress);
            }
            if (string.IsNullOrEmpty(UtilitesMethods.GetText(CreateNewOrderEC.ContactPhone)))
            {
                CreateNewOrderEC.ContactPhone.SendKeys(Constants.ContactPhone);
            }

            //Add New Shipping address 
            UtilitesMethods.WaitUntilElementExists();
            if (CreateNewOrderEC.ShippingAddressEdit.Enabled)
            {
                var element = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[2]/div[1]/main[1]/section[1]/div[1]/form[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/div[1]/*[name()='svg'][1]/*[name()='path'][1]"));
                Actions actions = new Actions(driver);
                actions.MoveToElement(element);
                actions.Perform();
                CreateNewOrderEC.ShippingAddressEdit.Click();
                CreateNewOrderEC.btnAddNewAddress.Click();
                CreateNewOrderEC.InputLocationName.Click();
                CreateNewOrderEC.SelectLocationName.Click();
                CreateNewOrderEC.SearchLocationAddress.SendKeys(Constants.Address);
                UtilitesMethods.WaitUntilElementExists();
                CreateNewOrderEC.SelectAddress.Click();
                UtilitesMethods.WaitUntilElementExists();
                CreateNewOrderEC.BtnSave.Click();
                UtilitesMethods.WaitUntilElementExists();
                CreateNewOrderEC.Donebtn.Click();

            }
            //Add New Billing address 
            if (CreateNewOrderEC.BillingAddressEdit.Enabled)
            {
                var element = driver.FindElement(By.XPath("//tr[@class='align-top relative']//div[@class='absolute cursor-pointer']//*[name()='svg']"));
                Actions actions = new Actions(driver);
                actions.MoveToElement(element);
                actions.Perform();
                CreateNewOrderEC.BillingAddressEdit.Click();
                CreateNewOrderEC.btnAddNewAddress.Click();
                CreateNewOrderEC.InputLocationName.Click();
                CreateNewOrderEC.SelectLocationName.Click();
                CreateNewOrderEC.SearchLocationAddress.SendKeys(Constants.Address);
                UtilitesMethods.WaitUntilElementExists();
                CreateNewOrderEC.SelectAddress.Click();
                UtilitesMethods.WaitUntilElementExists();
                CreateNewOrderEC.BtnSave.Click();
                UtilitesMethods.WaitUntilElementExists();
                CreateNewOrderEC.Donebtn.Click();

            }


            CreateNewOrderEC.TabGeneralDetails.Click();
            // clcik on order detials tab to enter order details
            CreateNewOrderEC.TabOrderDetails.Click();
            // Add new order item from DDL
            CreateNewOrderEC.BtnSelectOrderItems.Click();
            UtilitesMethods.WaitUntilElementExists();

            CreateNewOrderEC.SelectOrderItemsFromDDL.Click();
            UtilitesMethods.WaitUntilElementExists();

            if (CreateNewOrderEC.BtnDone.Displayed)
            {
                CreateNewOrderEC.BtnDone.Click();
            }
            else
            {
                Console.Write("Done Btn Button is not displayed");
            }
            UtilitesMethods.WaitUntilElementExists();
            CreateNewOrderEC.TabGeneralDetails.Click();
            if (string.IsNullOrEmpty(UtilitesMethods.GetText(CreateNewOrderEC.ContactPhone)))
            {
                CreateNewOrderEC.ContactPhone.SendKeys(Constants.ContactPhone);
            }
            // Click on Create Order Button
            CreateNewOrderEC.BtnCreateOrder.Click();
            UtilitesMethods.WaitUntilElementExists();


        }

        // Create new sales order with create new customer.
        [Test, Order(2), Category("CreateOrderTestCases")]
        public void CreateNewOrderWithNewCustomer()
        {
            UtilitesMethods.WaitUntilElementExists();
            if (CreateNewOrderEC.SalesOrderMenu.Displayed)
            {
                CreateNewOrderEC.SalesOrderMenu.Click();
            }
            else
            {
                Console.Write("clicked on sales order Menu on dashboard is not displayed");
            }
            if (CreateNewOrderEC.BtnCreateOrder.Displayed)
            {
                CreateNewOrderEC.BtnCreateOrder.Click();
            }
            else
            {
                Console.Write("Create Order Button is not displayed");
            }
            CreateNewCustomer.CreatenewCustomer.Click();
            CreateNewCustomer.CompanyName.SendKeys(Constants.CompanyName);
            Random generator = new Random();
            string r = generator.Next(0, 1000000).ToString("D6");
            CreateNewCustomer.AccountRef.SendKeys(Constants.AccountRef + r);
            CreateNewCustomer.ABN.SendKeys(Constants.ABN);
            CreateNewCustomer.PhoneNumber.SendKeys(Constants.PhoneNumber);
            CreateNewCustomer.FirstName.SendKeys(Constants.FirstName);
            CreateNewCustomer.LastName.SendKeys(Constants.LastName);
            CreateNewCustomer.EmailAddress.SendKeys(Constants.EmailAddress);
            CreateNewCustomer.MobileNumber.SendKeys(Constants.MobileNumber);
            CreateNewCustomer.LocationName.SendKeys(Constants.LocationName);
            CreateNewCustomer.Address.SendKeys(Constants.Address);
            UtilitesMethods.WaitUntilElementExists();
            CreateNewCustomer.SelectAddress.Click();
            UtilitesMethods.WaitUntilElementExists();
            CreateNewCustomer.SaveAddress.Click();
            UtilitesMethods.WaitUntilElementExists();
            CreateNewCustomer.btnCreate.Click();
            CreateNewCustomer.ClosePopup.Click();
            UtilitesMethods.WaitUntilElementExists();
            CreateNewOrderEC.TabOrderDetails.Click();
            // Add new order item from DDL
            CreateNewOrderEC.BtnSelectOrderItems.Click();
            UtilitesMethods.WaitUntilElementExists();
            CreateNewOrderEC.SelectOrderItemsFromDDL.Click();
            UtilitesMethods.WaitUntilElementExists();
            if (CreateNewOrderEC.BtnDone.Displayed)
            {
                CreateNewOrderEC.BtnDone.Click();
            }
            else
            {
                Console.Write("Done Btn Button is not displayed");
            }
            UtilitesMethods.WaitUntilElementExists();
            CreateNewOrderEC.BtnCreateOrder.Click();
            UtilitesMethods.WaitUntilElementExists();
        }

        // Process New sales order with existing customer from requried aatention tab.
        [Test, Order(3), Category("CreateOrderTestCases")]
        public void ProcessNewOrder()
        {
            Console.Write("Clicked on sales order menu" + Environment.NewLine);
            UtilitesMethods.WaitUntilElementExists();


            // clicked on sales order Menu on dashboard
            if (Processorder.SalesOrderMenu.Displayed)
            {
                Processorder.SalesOrderMenu.Click();
                Console.Write(Environment.NewLine + "clicked on sales order Menu on dashboard");
            }
            else
            {
                Console.Write("clicked on sales order Menu on dashboard is not displayed");
            }
            // clicked on Require Attention
            Processorder.RequireAttention.Click();
            Console.Write("clicked on Require Attention");
            UtilitesMethods.WaitUntilElementExists();
            //string noOrders = driver.FindElement(By.XPath("//td[@class='MuiTableCell-root jss140 MuiTableCell-body jss141 MuiTableCell-alignCenter']")).Text;


            //click on first row of the order table.
            Processorder.SelectOrder.Click();

            //click on ready to pick button
            Processorder.BtnReadytoPick.Click();
            UtilitesMethods.WaitUntilElementExists();

            //click on print pick slip button
            Processorder.BtnPrintPickSlip.Click();
            UtilitesMethods.WaitUntilElementExists();


            //close pick slip tab and swtich to main tab
            var tabs = driver.WindowHandles;
            if (tabs.Count > 1)
            {
                driver.SwitchTo().Window(tabs[1]);
                driver.Close();
                driver.SwitchTo().Window(tabs[0]);
            }
            if (Processorder.BtnOpenStatusbar.Enabled)
            {
                Processorder.BtnOpenStatusbar.Click();
            }

            // loop for filling pick qty in table
            var Pickordertable = driver.FindElement(By.TagName("table"));
            var Pickorderrows = Pickordertable.FindElements(By.TagName("tr"));

            foreach (var row in Pickorderrows)
            {
                var tds = row.FindElements(By.TagName("input"));
                foreach (var entry in tds)
                {
                    Console.WriteLine(entry.Text);
                    entry.SendKeys("" + 1);
                }

            }

            // click on move to packaging button
            Processorder.BtnMovetoPackaging.Click();
            UtilitesMethods.WaitUntilElementExists();
            if (Processorder.BtnOpenStatusbar.Enabled)
            {
                Processorder.BtnOpenStatusbar.Click();
            }
            // loop for filling pack qty in table
            var Packordertable = driver.FindElement(By.TagName("table"));
            var Packorderrows = Packordertable.FindElements(By.TagName("tr"));

            foreach (var row in Packorderrows)
            {
                var tds = row.FindElements(By.TagName("input"));
                foreach (var entry in tds)
                {
                    Console.WriteLine(entry.Text);
                    entry.SendKeys("" + 1);
                }

            }
            UtilitesMethods.WaitUntilElementExists();

            // click on pack button
            Processorder.BtnPack.Click();
            UtilitesMethods.WaitUntilElementExists();


            // click on ready to ship button
            Processorder.BtnReadytoShip.Click();
            if (Processorder.BtnOpenStatusbar.Enabled)
            {
                Processorder.BtnOpenStatusbar.Click();
            }
            UtilitesMethods.WaitUntilElementExists();


            // enter TrackingUrl and TrackingId
            Random generator = new Random();
            string r = generator.Next(0, 1000000).ToString("D6");
            Processorder.TrackingUrl.SendKeys(Constants.TrackingUrl + r);
            Processorder.TrackingId.SendKeys(r);

            // click on submit and ship button
            Processorder.BtnSubmitandShip.Click();
            if (Processorder.BtnOpenStatusbar.Enabled)
            {
                Processorder.BtnOpenStatusbar.Click();
            }


        }

        [TearDown]
        public void CleanUp()
        {
            //driver.Close();
            Console.Write("test case ended successfully");
        }
    }
}
